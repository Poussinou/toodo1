package com.wattwurm.toodoo

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.GestureDetectorCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
import com.wattwurm.toodoo.SwipeOnItemTouchListener.OnSwipeActionListener
import com.wattwurm.toodoo.data.*
import com.wattwurm.toodoo.databinding.DialogSearchBinding
import com.wattwurm.toodoo.databinding.FragmentTaskLstBinding
import kotlin.math.abs

class FragmentTaskLst : Fragment() {

    private val requestCodeImport= 123
    private val requestCodeImportFromTML = 456

    private lateinit var act: MainActivity
    private lateinit var binding: FragmentTaskLstBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        Log.i("DEBUG", "FragmentTaskLst.onCreateView")
        act = this.activity as MainActivity

        Log.i("DEBUG", "ListMode old: ${act.appState.tasks.listMode} ")
        Log.i("DEBUG", "ListMode new: ${act.appState.tasks.listModePending} ")
        // there may be a pending list mode, so first reset tasks in list
        act.appState.tasks.resetTaskListByListMode()

        // Inflate the layout for this fragment
        binding = FragmentTaskLstBinding.inflate(inflater, container, false)
        binding.taskLstHeader.setText(textHeader())
        binding.taskLstNumTasks.setText(textNumTasks())

        val itemsRecyclerView = binding.taskListInFragment
        itemsRecyclerView.addItemDecoration(
            DividerItemDecoration(
                act,
                DividerItemDecoration.VERTICAL
            )
        )

        val recyclerViewAdapter = TasksAdapter(act.appState.tasks)
        itemsRecyclerView.adapter = recyclerViewAdapter
        recyclerViewAdapter.notifyDataSetChanged()

        itemsRecyclerView.addOnItemTouchListener(
            SwipeOnItemTouchListener(act.applicationContext,
                itemsRecyclerView,
                object : OnSwipeActionListener {
                    override fun onLeftSwipe() {
                        doSwipe(FilterDirection.FORWARD)
                    }
                    override fun onRightSwipe() {
                        doSwipe(FilterDirection.BACKWARD)
                    }
                }
            )
        )

        itemsRecyclerView.addOnItemClickListener { position: Int, view: View ->
            act.appState.tasks.setCurrentPosition(position)
            act.showTaskDetail()
        }

        setHasOptionsMenu(true)
        registerForContextMenu(binding.taskListInFragment)  // needed for the context menu

        // handle back pressed for lists on second level, i.e. when list displays search results or tasks for one category
        act.onBackPressedDispatcher.addCallback(viewLifecycleOwner, object : OnBackPressedCallback(act.appState.tasks.listMode.isSecondLevelList) {
            override fun handleOnBackPressed() {
                act.appState.tasks.listModePending = ListMode.FILTERED_LIST
//                act.onBackPressed()       //  does NOT pop fragment from back stack, makes app freeze  :-(
//                requireActivity().onBackPressed()  // does NOT pop fragment from back stack either
                act.supportFragmentManager.popBackStack()
            }
        })

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_lst, menu)
        if(act.appState.tasks.listMode.isSecondLevelList) {
            menu.findItem(R.id.search).setVisible(false)
            menu.findItem(R.id.setFilter).setVisible(false)
            menu.findItem(R.id.setSortCrit).setVisible(false)
            menu.findItem(R.id.manageCategories).setVisible(false)
            menu.findItem(R.id.exportData).setVisible(false)
            menu.findItem(R.id.importData).setVisible(false)
//          menu.findItem(R.id.importTMLite).setVisible(false)
            if(act.appState.tasks.listMode == ListMode.STRING_SEARCH) {
                menu.findItem(R.id.new_task).setVisible(false)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.new_task -> {
                act.appState.tasks.setCreateItemPending()
                act.showTaskDetail()
                true
            }
            R.id.search -> {
                createDialogForSearch()
                true
            }
            R.id.setFilter -> {
                act.showFilters()
                true
            }
            R.id.setSortCrit -> {
                act.showSortCrit()
                true
            }
            R.id.exportData -> {
                act.checkAndWriteTasksToExt()
                true
            }
            R.id.importData -> {
                startIntentForFileRead(requestCodeImport)
                true
            }
//            R.id.importTMLite -> {
//                startIntentForFileRead(requestCodeImportFromTML)
//                true
//            }
            R.id.manageCategories -> {
                act.showCatList()
                true
            }
            R.id.about -> {
                act.createDialogAbout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu,
        v: View,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        // menu.setHeaderTitle("Select what to do with task")
        act.menuInflater.inflate(R.menu.menu_tsk_ctxt, menu)
        when (act.appState.tasks.currentTask.status) {
            CompletionStatus.OPEN -> {
                menu.findItem(R.id.resetOpen).setVisible(false)
                menu.findItem(R.id.deleteTask).setVisible(false)
            }
            CompletionStatus.DONE -> {
                menu.findItem(R.id.setDone).setVisible(false)
            }
        }
        if(act.appState.tasks.currentTask.dueDate == TDate.today) {
            menu.findItem(R.id.setDueDateToday).setVisible(false)
        }
        if(act.appState.tasks.currentTask.dueDate == TDate.tomorrow) {
            menu.findItem(R.id.setDueDateTomorrow).setVisible(false)
        }
        if(act.appState.tasks.currentTask.dueTime == null) {
            menu.findItem(R.id.deleteTime).setVisible(false)
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        val taskName = act.appState.tasks.currentTask.name
        return when (item.itemId) {
            R.id.editTask -> {
                act.showTaskDetail()
                true
            }
            R.id.deleteTask -> {
                act.appState.tasks.removeCurrentItem()
                // binding.taskListInFragment.adapter?.notifyItemRemoved(position)
                // this is not sufficient, will not update the positions in the adapter!!!
                binding.taskListInFragment.adapter?.notifyDataSetChanged()
                binding.taskLstNumTasks.setText(textNumTasks())
                Toast.makeText(
                    this.context,
                    "task $taskName deleted",
                    Toast.LENGTH_LONG
                ).show()
                act.writeTasks()
                true
            }
            R.id.setDueDateToday -> {
                act.appState.tasks.currentTask.dueDate = TDate.today
                afterItemChanged()
                true
            }
            R.id.setDueDateTomorrow -> {
                act.appState.tasks.currentTask.dueDate = TDate.tomorrow
                afterItemChanged()
                true
            }
            R.id.deleteTime -> {
                act.appState.tasks.currentTask.dueTime = null
                afterItemChanged()
                true
            }
            R.id.setDone -> {
                act.appState.tasks.currentTask.status = CompletionStatus.DONE
                afterItemChanged()
                true
            }
            R.id.resetOpen -> {
                act.appState.tasks.currentTask.status = CompletionStatus.OPEN
                afterItemChanged()
                true
            }
            else -> super.onContextItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == requestCodeImport) {
            if (resultCode == Activity.RESULT_OK) {
                val selectedfile: Uri? = data?.data    //The uri with the location of the file
                Log.i("DEBUG", "URI is $selectedfile")
                if (selectedfile != null) {
                    val inputStream = act.contentResolver.openInputStream(selectedfile)
                    if (inputStream != null) {
                        try {
                            act.appState.readAllDataFromStream(inputStream)
                            binding.taskListInFragment.adapter?.notifyDataSetChanged()
                            binding.taskLstHeader.setText(textHeader())
                            binding.taskLstNumTasks.setText(textNumTasks())
                            act.writeCategories()
                            act.writeTasks()
                            act.writeFilters()
                            act.writeSortCrit()
                            Toast.makeText(this.context, "imported ${act.appState.categories.countItems} categories and ${act.appState.tasks.countAllTasks} tasks ", Toast.LENGTH_LONG).show()
                        } catch (e: Exception) {
                            Toast.makeText(this.context, "error importing data $e", Toast.LENGTH_LONG).show()
                        } finally {
                            inputStream.close()
                        }
                    } else {
                        Log.i("DEBUG", "selected file $selectedfile could not be resolved to inputstream")
                    }
                } else {
                    Log.i("DEBUG", "selected file $selectedfile does not exist")
                }
            } else {
                Log.i("DEBUG", "onActivityResult requestCode $requestCode (import data) got resultCode $resultCode")
            }
        } else if (requestCode == requestCodeImportFromTML) {
            if (resultCode == Activity.RESULT_OK) {
                val selectedfile: Uri? = data?.data    //The uri with the location of the file
                Log.i("DEBUG", "URI is $selectedfile")
                if (selectedfile != null) {
                    val inputStream = act.contentResolver.openInputStream(selectedfile)
                    if (inputStream != null) {
                        try {
                            act.appState.readTMLiteDataFromStream(inputStream)
                            binding.taskListInFragment.adapter?.notifyDataSetChanged()
                            binding.taskLstHeader.setText(textHeader())
                            binding.taskLstNumTasks.setText(textNumTasks())
                            // todo for test inactivated
//                            act.writeCategories()
//                            act.writeTasks()
//                            act.writeFilters()
//                            act.writeSortCrit()
                            Toast.makeText(this.context, "imported ${act.appState.categories.countItems} categories and ${act.appState.tasks.countAllTasks} tasks from TML file", Toast.LENGTH_LONG).show()
                        } catch (e: Exception) {
                            Toast.makeText(this.context, "error importing TML data $e", Toast.LENGTH_LONG).show()
                        } finally {
                            inputStream.close()
                        }
                    } else {
                        Log.i("DEBUG", "selected file $selectedfile could not be resolved to inputstream")
                    }
                } else {
                    Log.i("DEBUG", "selected file $selectedfile does not exist")
                }
            } else {
                Log.i("DEBUG", "onActivityResult requestCode $requestCode (import data from TML) got resultCode $resultCode")
            }
        }
    }

    private fun textHeader(): String {
        return when (act.appState.tasks.listMode) {
            ListMode.FILTERED_LIST -> act.appState.tasks.filters.displayInOverview.let { if (it.isNotBlank()) "tasks filtered by $it" else "all tasks" }
            ListMode.STRING_SEARCH -> "search result for: ${act.appState.tasks.searchCrit.sstring}"
            ListMode.SINGLE_CATEGORY -> "tasks for category: ${act.appState.tasks.filterCategory.name}"
        }
    }

    private fun textNumTasks(): String {
        val numberAllTasks = act.appState.tasks.countAllTasks
        val numberSelTasks = act.appState.tasks.countSelectedTasks
        return "${numberSelTasks} of ${numberAllTasks}"
    }

    private fun afterItemChanged() {
        val taskName = act.appState.tasks.currentTask.name
        act.appState.tasks.onTaskChanged()
        binding.taskListInFragment.adapter?.notifyDataSetChanged()  // this might cause the current task to get out of the list
        binding.taskLstNumTasks.setText(textNumTasks())
        Toast.makeText(
            this.context,
            "changes for task ${taskName} saved",
            Toast.LENGTH_LONG
        ).show()
        act.writeTasks()
    }

    private fun doSwipe(direction: FilterDirection) {
        when (act.appState.tasks.listMode) {
            ListMode.FILTERED_LIST ->  {
                if (act.appState.tasks.filters.dueDateFilter is SingleFilter1) {
                    act.appState.tasks.filters.moveDueDateFilter(direction)
                    act.appState.tasks.applyFilters()
                    binding.taskListInFragment.adapter?.notifyDataSetChanged()  // the new tasks are displayed
                    binding.taskLstHeader.setText(textHeader())
                    binding.taskLstNumTasks.setText(textNumTasks())
                }
            }
            ListMode.SINGLE_CATEGORY ->  {
                val oldCategory = act.appState.categories.currentItem
                act.appState.categories.moveCategory(direction)
                val newCategory = act.appState.categories.currentItem
                if (newCategory != oldCategory) {
                    act.appState.tasks.filterCategory = newCategory
                    act.appState.tasks.refreshTaskList()
                    binding.taskListInFragment.adapter?.notifyDataSetChanged()  // the new tasks are displayed
                    binding.taskLstHeader.setText(textHeader())
                    binding.taskLstNumTasks.setText(textNumTasks())
                }
            }
            ListMode.STRING_SEARCH -> { // do nothing, in search mode no swiping
            }
        }
    }

    private fun getNextCategory(direction: FilterDirection) : Category{
        val currentCategory = act.appState.categories.currentItem
        while (true) {  // loop stops when next category with tasks was found, or when finally all categories have been tried and we come back to the currentCategory
            act.appState.categories.moveCategory(direction)
            val category = act.appState.categories.currentItem
            if (act.appState.tasks.tasksExistingForCategory(category) || category == currentCategory) {
                // return the next category that has tasks
                // if that does not exist, return the currentCategory to avoid endless loop
                return category
            }
        }
    }

    private fun startIntentForFileRead(requestCode: Int) {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.setType("text/xml")
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        startActivityForResult(intent, requestCode)
    }

    private fun createDialogForSearch() {
        val dialogSearchBinding = DialogSearchBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this.context)
        builder
            .setMessage("Search for:")
            .setView(dialogSearchBinding.root)
            .setPositiveButton("Search") { dialog, id -> }
            .setNegativeButton("Cancel") { dialog, id -> }
        val dialog = builder.create()

        // makes the keyboard appear
        //dialog.window?.setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
        dialog.show()
        dialogSearchBinding.searchText.setText(act.appState.tasks.searchCrit.sstring)
        dialogSearchBinding.searchText.setSelection(0, dialogSearchBinding.searchText.text.length) // highlights the whole search text

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener { view ->
            val searchString = dialogSearchBinding.searchText.text.toString().trim()
            if (searchString.isBlank()) {
                // show error message, and do not close
                dialogSearchBinding.searchMessage.text = "Search string must not be blank!"
            } else {
                val option = when (dialogSearchBinding.nameOrDesc.checkedRadioButtonId) {
                    dialogSearchBinding.nameOnly.id -> SearchOption.NAME_ONLY
                    dialogSearchBinding.descOnly.id -> SearchOption.DESC_ONLY
                    else -> SearchOption.NAME_AND_DESC
                }
                // makes the keyboard disappear
                // dialog.window?.setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_HIDDEN)  // does not work
                imm?.hideSoftInputFromWindow(dialogSearchBinding.root.windowToken, 0)
                dialog.dismiss()

                // prepare showing results of string search, i.e. second level list
                act.appState.tasks.listModePending = ListMode.STRING_SEARCH
                act.appState.tasks.searchCrit = SearchCrit(searchString, option)
                act.showTaskListSecondLevel()
            }
        }

        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener { view ->
            imm?.hideSoftInputFromWindow(dialogSearchBinding.root.windowToken, 0)
            dialog.dismiss()
        }
    }
}

class SwipeOnItemTouchListener(
    context: Context?,
    recyclerView: RecyclerView,
    private val mOnSwipeActionListener: OnSwipeActionListener
) : OnItemTouchListener {

    interface OnSwipeActionListener {
        fun onLeftSwipe()
        fun onRightSwipe()
    }

    companion object {
        private const val SWIPE_MIN_DISTANCE = 200
        private const val SWIPE_VELOCITY_THRESHOLD = 300
    }

    private val mGestureDetector: GestureDetectorCompat = GestureDetectorCompat(context, object : SimpleOnGestureListener() {
        // GestureDetectorCompat newer than GestureDetector

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            Log.i("DEBUG", "SimpleOnGestureListener onFling ${e1} ${e2}")
            var result = false
            try {
                val diffY = e2.y - e1.y
                val diffX = e2.x - e1.x
                if (abs(diffX) > abs(diffY)) {
                    // swipe horizontal
                    if (abs(diffX) > SWIPE_MIN_DISTANCE && abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
//                            Log.i("DEBUG", "SimpleOnGestureListener onFling onRightSwipe")
                            mOnSwipeActionListener.onRightSwipe()
                        } else {
//                            Log.i("DEBUG", "SimpleOnGestureListener onFling onLeftSwipe")
                            mOnSwipeActionListener.onLeftSwipe()
                        }
                        result = true  // todo better return false?
                    }
                    // swipe vertical not relevant here
                }
            } catch (exception: Exception) {
                Log.i("DEBUG", "SimpleOnGestureListener exception $exception")
                exception.printStackTrace()
            }
            return result
        }
    })

    override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
        Log.i("DEBUG", "SwipeOnItemTouchListener onInterceptTouchEvent MotionEvent ${e}")
        mGestureDetector.onTouchEvent(e)  // todo  why is the mGestureDetector called here, not in onTouchEvent ?
        return false  // todo why return false, not true?
    }

    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
        Log.i("DEBUG", "SwipeOnItemTouchListener onTouchEvent MotionEvent ${e}")
        // do nothing
    }
    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        // do nothing
    }
}


fun RecyclerView.addOnItemClickListener(onClick: (position: Int, view: View) -> Unit) {
    this.addOnChildAttachStateChangeListener(
        object : RecyclerView.OnChildAttachStateChangeListener {

            override fun onChildViewDetachedFromWindow(view: View) {
                view.setOnClickListener (null)
            }

            override fun onChildViewAttachedToWindow(view: View) {
                view.setOnClickListener {
                    val holder = getChildViewHolder(view)
                    onClick(holder.adapterPosition, view)
                }
            }
        }
    )
}
