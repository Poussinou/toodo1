package com.wattwurm.toodoo

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.wattwurm.toodoo.data.FilterAll
import com.wattwurm.toodoo.data.MultipleFilter
import com.wattwurm.toodoo.data.MultipleFilterM
import com.wattwurm.toodoo.databinding.FilteroptionItemCBinding

class MultipleFilterAdapter<T : Comparable<T>>(private val options: List<T>, var currentFilter: MultipleFilter<T>) : RecyclerView.Adapter<MultipleFilterAdapter.FilterOptionHolder>(){

    class FilterOptionHolder(val binding: FilteroptionItemCBinding) : RecyclerView.ViewHolder(binding.root)

    private var forFilterAllCheckAllOptions = true

    override fun getItemCount(): Int {
        return options.size + 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterOptionHolder {
        val binding = FilteroptionItemCBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FilterOptionHolder(binding)
    }

    override fun onBindViewHolder(holder: FilterOptionHolder, position: Int) {

        // special handling for special checkbox [all]:
        // all button toggles all entries (on / off)
        // temporarily display no entry checked (but this also counts as FilterAll)

        if (position == 0) {
            // for first option <all> add a line to separate it from other options
            holder.binding.root.setBackgroundResource(R.drawable.shape_line_bottom)
            holder.binding.filterItemText.text = FilterAll.display
            holder.binding.filterItemCheckBox.isChecked = (currentFilter == FilterAll && forFilterAllCheckAllOptions)
        } else {
            holder.binding.root.setBackgroundResource(0)
            val currentItem = options[position-1]
            holder.binding.filterItemText.text = currentItem.toString()
            val filterOld = currentFilter
            holder.binding.filterItemCheckBox.isChecked = (filterOld is MultipleFilterM && filterOld.options.contains(currentItem))
                    || (filterOld is FilterAll && forFilterAllCheckAllOptions)
        }

        holder.binding.filterItemCheckBox.setOnClickListener { view ->
            if (position == 0) {
                // special filter option [all]
                if (currentFilter == FilterAll) {
                    forFilterAllCheckAllOptions = !forFilterAllCheckAllOptions
                } else {
                    currentFilter = FilterAll
                    forFilterAllCheckAllOptions = true
                }
            } else {   //  if (position > 0)
                val currentItem = options[position-1]
                val filterOld = currentFilter

                when(filterOld) {
                    FilterAll -> {
                        currentFilter = if(forFilterAllCheckAllOptions) MultipleFilterM(options.filter { it != currentItem })
                        else MultipleFilterM(listOf(currentItem))
                    }
                    is MultipleFilterM -> {
                        if (filterOld.options.contains(currentItem)) {
                            if(filterOld.options.size == 1) {
                                currentFilter = FilterAll
                                forFilterAllCheckAllOptions = false
                            } else {
                                val newOptions = filterOld.options.filter { it != currentItem }.sorted()
                                currentFilter = MultipleFilterM(newOptions)
                            }
                        } else {
                            if(filterOld.options.size == options.size - 1) {
                                currentFilter = FilterAll
                                forFilterAllCheckAllOptions = true
                            } else {
                                val newOptions = (listOf(currentItem) + filterOld.options).sorted()
                                currentFilter = MultipleFilterM(newOptions)
                            }
                        }
                    }
                }
            }
            this.notifyDataSetChanged()
        }
    }
}
