package com.wattwurm.toodoo.data

import java.lang.Exception
import java.util.*

data class TTime (val hour: Byte, val minute: Byte)  : Comparable<TTime>{
    init {
        if (hour < 0 || hour > 23) throw Exception("invalid hour")
        if (minute < 0 || minute > 59) throw Exception("invalid minute")
    }

    companion object {
        fun fromStorageRep(rep: String): TTime? {
            return if (rep.isEmpty()) null else TTime(
                rep.substring(
                    0,
                    2
                ).toByte(), rep.substring(3, 5).toByte()
            )
        }
        val now: TTime get() {
                val nowCalender = Calendar.getInstance() // today
                val hour = nowCalender.get(Calendar.HOUR_OF_DAY).toByte()
                val minute = nowCalender.get(Calendar.MINUTE).toByte()
                return TTime(hour, minute)
            }
        val nextFullHour: TTime get() {
            val nextHour = TTime.now.hour.let {
                if (it == 23.toByte()) 0 else it+1
            }
            return TTime(hour=nextHour.toByte(), minute=0)
        }
    }

    fun toStorageRep(): String {
        val hourString = "00${hour}".takeLast(2)
        val minuteString = "00${minute}".takeLast(2)
        return "${hourString}:${minuteString}"
    }

    override fun compareTo(other: TTime): Int {
        (this.hour.compareTo(other.hour)).let { if (it != 0) return it }
        (this.minute.compareTo(other.minute)).let { if (it != 0) return it }
        return 0
    }

}
