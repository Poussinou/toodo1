<h3>Features of toodoo:</h3>

<b>Tasks:</b>
You can create, change and delete tasks.
Each task has a name, a category, a priority (HIGH, MEDIUM, or LOW), and a completion-status (i.e. it is either OPEN or DONE).
Optionally, each task can also have a description (arbitrary text), a due_date and a due_time.
A task can only be deleted if its completion status is DONE, in order to avoid unintentional deletion.

<b>Categories:</b>
You can create as many categories as you need.
The category a task belongs to can be changed.
A category can be renamed, which will then be visible in all tasks belonging to that category.
Unused categories can be deleted, i.e. deletion is only possible if the category has no tasks, and if it is not used in the current filter.

<b>Filtering tasks:</b>
The tasks to be displayed in the list can be selected by
- completion_status
- due_date/due_time
- priority
- category
or any combination of these.

For due_date there are several predefined filter options, e.g. TODAY, CURRENT_WEEK, OVERDUE, TASKS_WITHOUT_DATE, etc.
For priority and category, it is possible to filter with more than one option. E.g. you can choose to show tasks with priorities HIGH or MEDIUM. Or you can display all tasks that have either category "Family" or "Friends".

<b>Sorting tasks:</b>
The task list can be sorted by:
- completion_status
- due_date/due_time
- priority
- category
- name

Any sort order is possible. E.g. first by due_date/time, then by completion status, category, priority and name. Or any other order you prefer.

<b>Searching tasks:</b>
Tasks can be searched by text. The search text you enter can be applied to the task name, the task description, or both.

<b>Convenience:</b>
There are shortcuts for often used actions. E.g.
From the context menu of a category you can jump to a list of all tasks for that category.
From the context menu of a task you can change its completion_status, set it to DONE (or OPEN respectively), or set its due date to today or tomorrow.
When the task list is filtered by "tasks due TODAY", you can easily move to the previous / next day by swiping right / left.
Similarly, when the task list is filtered by "tasks due CURRENT_WEEK", you can easily move to the previous / next week by swiping.

<b>Import / Export:</b>
All user data (i.e. tasks, categories, filters and sort criteria) can be exported to an external file (xml-format).
Vice versa, data can be imported from an external file.

<b>Technical:</b>
Your data is stored locally on your device.
No connection to the internet needed.
No ads. No tracking.

<h3>What toodoo can not do:</h3>
Features, toodoo does not have (not yet, may be added in future releases):

<b>No Multi-language</b>
User interface is currently in English only.

<b>No bulk operations</b>
Actions (update, delete, ...) are always applied to a single task.
E.g. it is not possible to delete all tasks with status DONE with one click.

<b>No options</b>
Settings are currently fixed, cannot be adapted. E.g.
- date format is yyyy-mm-dd, time format is 24 hours.
- first day of week is monday.
- default due_date for new tasks is TODAY.
- when sorting by due_date/time, within a day, tasks without time are sorted behind tasks with time.

<b>Folder and file-name for export data are fixed</b>
Exported data is stored in folder /Download/toodoo. File is named according to scheme toodooData-yyyy-mm-dd-hh-mm-ss.
